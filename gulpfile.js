
 var elixir = require('laravel-elixir');

/** 
Created : 14-10-2015 
Author: Jefta Chibiya 

// Please note that the Gulpfile is provided as part of the framework, the user is required to
   fill in the required instructions (JC)

The gulpfile instructs the 'Gulp' File Dependency Manager which files to consolidate together, 
into individual files. 

*/

// Code in green represents paths to files within the resources/assets folder (Jefta Chibiya)

elixir(function(mix) {
    mix.sass('app.scss')
        .copy('resources/assets/bower/font-awesome/fonts','public/build/fonts')
        .browserSync({
    	proxy: 'sensationbarbers.app'
    	});
     
// Move the 'sweetalert.css' into: 'public/css/misc.css' (Jefta Chibiya)
    mix.styles(
        [
        '../bower/sweetalert/dist/sweetalert.css',
        '../bower/lity/dist/lity.min.css'
        ], 
        'public/css/misc.css');

// Consolidate js files from the resources folder into the file path: 'public/js/app.js'
    mix.scripts([
    	'../bower/jquery/dist/jquery.js',
	    '../bower/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        '../bower/sweetalert/dist/sweetalert.min.js',
        '../bower/lity/dist/lity.min.js',
        '../js/sens.js',
    	],'public/js/app.js');

// Create new versions of 'app.css' and 'app.js' files per each new save!  
    mix.version(['css/app.css','js/app.js']);
});
