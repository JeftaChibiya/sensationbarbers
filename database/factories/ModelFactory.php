 <?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
| This is a default model. It is a convinient method for Testing the scalability of a database or   the performance of an application (JC, C3398189)
|
*/

// The Model Factory for the Users table, to create test data for the users table 

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->username,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Service::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->word, 
		'short_description' => $faker->paragraphs(2);
		'price'=> $faker->numberBetween(1, 10), 
		'rating'    => $faker->numberBetween(1,5)
	];
});
}


