<?php

use Illuminate\Database\Seeder;
use App\News;
use Faker\Factory;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        foreach (range(1, 50) as $index) {
            News::create([
                'id' => $index,
                'title' => $faker->name,
                'body' => $faker->text(5000),
                'link' => $faker->url,
                'slug' => $faker->slug,
            ]);

        }
    }
}
