<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // Here roles are delegated to particular users 
        // Role No.1 is the adminstrator, 2 is Owner and 3 is a member of staff 

        Role::create(['username'=>'administrator']);
        Role::create(['username'=>'owner']);
        Role::create(['username'=>'staff']);
        
        // Attach the roles to the Users within Users table. 
        // Give role No1 or of Adminstrator to User number 1 in the Users table 
        
        User::first()->roles()->attach(1);

    }
}
