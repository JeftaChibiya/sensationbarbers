<?php

use Illuminate\Database\Seeder;
use App\Barber;
use Faker\Factory;

class BarberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        foreach (range(1, 50) as $index) {
            Barber::create([
                'id' => $index,
                'name' => $faker->name,
            ]);

        }

    }
}
