<?php

use App\Service;
use Illuminate\Database\Seeder;
use Faker\Factory;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

            Service::create([
                'name'      => 'Haircuts',
                'short_description' => 'Find all hairstyles for kids and adults here',
                'price'     => '8 on average',
                'rating'    => $faker->numberBetween(1,5),
            ]);

             Service::create([
                'name' => 'Pattern Designs',
                'short_description' => 'Styles for your hair',
                'price' => '£8.00',
                'rating' => $faker->numberBetween(1,5),
            ]);

             Service::create([
                'name' => 'Afro',
                'short_description' => 'Afro shape ups',
                'price' => '£8.00',
                'rating' => $faker->numberBetween(1,5),
            ]);

             Service::create([
                'name' => 'Afro and Euro',
                'short_description' => 'All types',
                'price' => '£8.00',
                'rating' => $faker->numberBetween(1,5),
            ]);
        }
    }
