<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            User::create([
                'username' => 'admin',
                'email' => 'admin@barbers.com',
                'password' => Hash::make('password'),
            ]);

            User::create([
                'username' => 'customer',
                'email' => 'customer@barbers.com',
                'password' => Hash::make('password'),
            ]);

            User::create([
                'username' => 'staff',
                'email' => 'staff@barbers.com',
                'password' => Hash::make('password'),
            ]);
    }
}
