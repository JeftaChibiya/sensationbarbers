<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Created on: 23-11-2015 by Jefta Chibiya, C3398189 

class CreateCategoriesTable extends Migration
{
    /**
     * Create table catergories
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Drop table 'categories'
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
