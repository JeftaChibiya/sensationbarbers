<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Created on: 15-03-2015, by Jefta Chibiya, C3398189 
// This is the Profiles table, to contain Profiles of the users 

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Profiles Table 

        Schema::create('profiles', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('twitter_username')->nullable();
            $table->text('bio')->nullable();
            $table->timestamps();
        });

        // Now to think about how I attach a profile to a user

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('profiles');

    }
}
