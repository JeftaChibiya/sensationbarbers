<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateServicePhotosTable extends Migration
{
    /**
     * 
     *
 * @return void
 */
public function up()
{
Schema::create('service_photos', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('service_id')->unsigned();
    $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
    $table->string('path');
    $table->timestamps();
});
}

    /**
     * 
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_photos');
    }
}
