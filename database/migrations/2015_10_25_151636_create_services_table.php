
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Create table (JC, C3398189)
     * Created on: 31-10-2015 
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('short_description');
            $table->integer('price');
            $table->integer('rating');
            $table->timestamps();
            
        });
    }
    // ! NB
    // Field for an image not included in this table as a field
    // Would only be suitable for one photo 
    // In the likely event of a service with multiple images, this leads to duplication of columns 
    // $table->string('photo1.jpg', 'photo2.png');
    // Separate table created for Service_images

    /**
     * Drop Table (JC)
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
