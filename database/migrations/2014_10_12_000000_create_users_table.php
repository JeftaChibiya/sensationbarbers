<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Created on: 15-10-2015, as part of the Laravel Installation 
// Courtesy of Laravel 

class CreateUsersTable extends Migration
{
    /**
     * Create Table Users (JC)
     *
     * @return void
     */


    // a table consists of an up function for CREATE TABLE, and a down method for DROP TABLE

    public function up()
    
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username'); 
            $table->string('email')->unique(); 
            $table->string('password'); 
            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Drop Table Users (JC)
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
