<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'features',
        'short_description',
        'is_reviewed',
        'is_published',
    ];


    /**
     * @param $is_reviewed
     * @return string
     */
    public function getIsReviewedAttribute($is_reviewed)
    {
        return ($is_reviewed == 1) ? 'Yes' : 'No';
    }


    /**
     * @param $is_published
     * @return string
     */
    public function getIsPublishedAttribute($is_published)
    {
        return ($is_published == 1) ? 'Yes' : 'No';
    }


    /**
     * @return string
     */
    public function getPriceAttribute($price)
    {

        if ($price == 0) {
            return 'P.O.A';
        }
        return '£' . number_format($price);

    }

    /**
     * @return array
     */
    public function getOriginalPriceAttribute()
    {
        return $this->getOriginal('price');
    }

    /**
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('is_reviewed', '=', 1)
            ->orWhere('is_published', '=', 1);
    }

    public function addPhoto(Photo $photo)
    {
        return $this->photos()->save($photo);
    }

    /**
     * A letting is composed of many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function photo()
    {
        return $this->hasOne(Photo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user;
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function ($listing) {

            $listing->slug = str_slug($listing->name);

            $latestSlug = static::whereRaw("slug RLIKE'^{$listing->slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug');

            if ($latestSlug) {
                $pieces = explode('-', $latestSlug);

                $number = intval(end($pieces));

                $listing->slug .= '-' . ($number + 1);
            }

        });
    }


}
