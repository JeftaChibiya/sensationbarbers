<?php

namespace App;

use App\Role;

use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*
     * This is the User model, pre-installed by Laravel
     * A Model is a layer that represents the logical or knowledge of the application
     * A model does not understand Http (Way, 2013)
     */

    // Function fillable - field which receives user input (JC)
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * 
     *
     * @var array
     */
    protected $hidden = [

        //password hidden for security (JC)
        'password', 'remember_token',
    ];

    // one user has one profile. 'Profile' refers to the name of the Profile model (jc)

    public function profile()  
    {   
        return $this->hasOne('Profile');
    }


    /**
     * @return 
     
     *  A user is given a role to determine what priviledges they have
     *  The statement below states that if there is a request for roles return all roles (JC) 
     *  Class 'User' owns many other Models (JC)
     */

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function hasRole($username)
    {
        foreach ($this->roles as $role) {
            if ($role->username == $username) return true;
        }

        return false;
    }

   
    public function assignRole($role)
    {
        // A SuperAdmin, user no1 has the priviledges of assigning roles to other users
        return $this->roles()->attach($role);

    }

    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

}
