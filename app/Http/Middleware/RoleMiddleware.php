<?php

namespace App\Http\Middleware;

//After namespacing, the second line references the 'Role' model
// the 'Role' model checks if a user has been assigned any special priviledges

use Closure;
use App\Role;
use Illuminate\Support\Facades\Auth;


class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        //if the user does not have a role, redirect back to home page
        if(Auth::guest() or !$request->user()->hasRole($role) ){
            return redirect('/');
        }

        return $next($request);
    }
}
