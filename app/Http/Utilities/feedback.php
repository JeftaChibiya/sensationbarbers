<?php
namespace App\Http\Utilities; 

class Feedback{
	protected static $feedback = [
		  "Complaint"   	=> '1', 
		  "Satisfaction" 	=> '2',
	];

	public static function all() { // to make this function 'static'
		return array_keys(static::$feedback);
	}

}
