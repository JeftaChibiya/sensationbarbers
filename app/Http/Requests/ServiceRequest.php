<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceRequest extends Request
{
    /**
     * Authorise request.
     *
     * @return
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'service_name'          =>  'required', 
            'short_description'     =>  'required',
            'price'                 =>  'required',
            'rating'                =>  'required',
        ];
    }

}
