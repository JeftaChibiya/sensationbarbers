<?php

// This is the Admin Controller, responsible for logging in a user who has an administrative role

// Created on: 17-03-2016

// the Admin extends the BaseController (JC, C3398189)
namespace App\Http\Controllers;

// Requests 
use Illuminate\Http\Request;

// The 'Role' mode is included here to specify to the controller which users will have the role of admin (JC, C3398189)
use App\Role;

// 'User' model is included here to provides registered users. It is then passed onto the count() function on line 38. 
use App\User;

// Yajra Datatables are a courtesy and property of Arjay Angeles. Designed to handle backend data presentation in the form of a data table 

//use Yajra\Datatables\Facades\Datatables;

use App\Http\Requests;


class AdminController extends Controller
{
	// The _construct method calls the middleware, which demands that the user is logged in before proceeding
  // Middleware are components which handle requests and responses, controlling user access before or after a component

    public function __construct()
    {
        $this->middleware('role:administrator');
        
    }

    public function dashboard()
    {   
      /* 
      // The variable '$users_count' contains occurences of all Users in the relationship stated by the User model 
      // The result is passed on to the PHP function; 'count()'
      */

  	$users_count = User::all()->count();

    return view('admin.dashboard')->with(
        [
           'users_count'=>$users_count
        ]

      // flash()->success('Success!', 'You are logged in');

        );
    }
    
    // Function 'getUsers' utilises the Yajra Datatables, and displays them to the Adminstrator 
    // Created on 28-08-2016, following the guidelines by the owner on the address provided. 
    // https://github.com/yajra/laravel-datatables
    
    // public function getUsers()
    // {
    //   $users = User::all();
    //           return Datatables::of($user)
    //         ->addColumn('edit', function ($user) {
    //             return editButton($user->id, 'user');
    //         })->addColumn('delete', function ($user) {
    //             return deleteButton($user->id, 'user');
    //         })->addColumn('view', function ($user) {
    //             return viewButton($user->id, 'user');
    //         })
    //         ->make(true);
    // }

}
