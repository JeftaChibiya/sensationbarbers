<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Service;

use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function services()
    {   
        $services = Service::paginate(3);    
        return view('services', compact('services'));

    }

    public function contact()
    {
        return view('contact');
    }

    public function news()
    {
        return view('news');
    }
    
    public function booking()
    {
        return view('bookings.make');
    }
}
