<?php

namespace App\Http\Controllers\Auth;

// User Model 
use App\User;

use Validator;
// Requests 
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Controllers\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors (Otwell, 2015)
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:25',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:4|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    // Code below added: 19/03/2015 
    // Courtesy of Jan Betzing
    // https://rck.ms/laravel-authentication-success-flash-messages/

    // protected function authenticated(Request $request, $user){
    //     $successmessage = 'Hey' Auth::user()->username.',you are logged in!';
    //     $request->session()->flash('success', $successmessage);
    //     return redirect()->intended($this->redirectedPath());
    // }

    // protected function getLogout()
    // {
    //     Auth::logout();
    //     session()->flash('success', 'You are logged out');
    //     return redirect(property_exists($this, 'redirectedAfterlogout')?$this->$thisredirectAfterlogout : '/');
    // }
}