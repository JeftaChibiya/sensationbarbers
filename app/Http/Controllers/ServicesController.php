<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\ServiceRequest;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Service;


class ServicesController extends Controller
{

    // /**
    //  * @return \Illuminate\Http\Response
    //  */

    // public function _construct()
    // {
    //     $this->middleware('auth', ['except' => ['show']]);

    // }

    /** Create new service 
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

        return view('services.create');
        
    }


    /**
     * @param  name
     * @return service.show
     */

    public function show($name)
    {   
         $service = Service::locatedAt($name)->first();

         return view('service.show', compact('service'));
    }
    

    public function addPhoto($name, Request $request)
    {

        $file = $request->file('photo');

        $name = time() . $file->getClientOriginalName();

        $file->move('service/photos', $name); 

        $service = Service::locatedAt($name)->first();

        $service->photos()->create(['path'=>"/service/photos/{$name}"]);

        return 'Done';

    }


    public function store(Request\ServiceRequest $request)
    {
        $this->validate();

    }


    /**
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update 
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }



    public function destroy($id, Service $service)
    {
        $service->delete();

        return redirect('/dashboard');
    }

}
