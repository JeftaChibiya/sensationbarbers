<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;


class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource (LARAVEL)
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // '::' represents a static method of reference 
        $profile = User::all();
        return view('profile.profile', compact('profile'));
    }

    // Courtesy of Jeffrey Way (2014): 
    // https://github.com/laracasts/Building-User-Profiles/blob/master/app/controllers/
    // profile/username edit 

    public function show()
    {   
        $user = $this->User($username);

        // corresponds to the view in the folder profile 
        return view('profile.show')->withUser($user);

    }
        // try
        // {
        // //   // Where the user exists!
        // $user = User::whereUsername($username)->firstOrFail();

        // }

        // catch(ModelNotFoundException $e)
        // {
        //    return redirect::home();

        // }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($username)
    {
        $user = $this->User($username);

        return View::make('profiles.edit')->with($user);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $profile = delete();
        return redirect('/admin/profile');
    }
}
