<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;

// References to the Models: Service, News and Barber
use App\Service;
use App\News;
use App\Barber;

class HomeController extends Controller
{

    public function index()
    {   
        // flash()->overlay('Welcome', 'Thank you for coming here'); 
        
        $news  		= News::paginate(4);
        $barbers  	= Barber::paginate(4);
        $services 	= Service::paginate(5);

        return view('home',compact('barber','services', 'news'));

    }
}
