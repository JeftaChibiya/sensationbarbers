<?php

  namespace  App\Http;
    // This function is specific to the web layer of the application 

    // this is then referenced within the FlyersController 
    // Passing a flash_message key to the session, and binding a message to the key 
    // the values of $title and message are passed onto the message function 
  class Flash {

    // $flash_message->message('Hello There') to display a message 
   
    // function 'create' acts as a blueprint on which other messages base their attributes on
    // It accepts a title, message and a level or type of message which inludes a corresponding icon 
    // Create is a master method 
    // the key is overwrite, the key can now be changed within the flash view to the suit the type of view  
    public function create($title, $message, $level, $key = 'flash_message')
    {
            return session()->flash($key,[
                'title'    => $title,
                'message'  => $message, 
                'level'    =>  $level
                ]);
    }
    // all these functions after the one above are abbreviated 
    // A demonstration of Laravel's level of abstraction
    
    public function info($title, $message, $key)
    {
        return $this->create($title, $message, 'info');
    }
    //for success 
    public function success($title, $message)
    {
        return $this->create($title, $message, 'success');
    }

    // Another alternative: public function _call($level, $args) //captures errors 
    public function error($title, $message)
    {
        return $this->create($title, $message, 'error');
    }
    
    // $flash->overlay() with success as the default level 
    public function overlay($title, $message, $level ='success')
    {
        return $this->create($title, $message, $level,'flash_message_overlay');
    }




    //  This is the alternative and more explicit form of the method above 
    // public function success($title, $message)

    // {
    //      session()->flash('flash_message', [
    //          'title'    => $title,
    //          'message'  => $message, 
    //          'level'    =>'error'
    //          ]);
    // }

  }