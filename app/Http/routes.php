<?php

//Service routes
Route::get('{service_name}', 'ServicesController@show');
Route::post('{service_name}/photos', 'ServicesController@addPhoto');

// Pages
Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'HomeController@index');
    Route::get('services', 'PagesController@services');
    Route::get('contact', 'PagesController@contact');
    Route::get('news', 'PagesController@news');  
    Route::auth(); 
    
});

// Admin Access only 
Route::group(['middleware' => ['role:administrator']], function () {
    Route::get('admin', 'AdminController@dashboard');
    Route::resource('admin/services', 'ServicesController');
    Route::resource('admin/users', 'UsersController');
    Route::resource('admin/news', 'newsController');
    Route::resource('admin/bookings', 'BookingsController');
});

// User profiles 
Route::get('/{profile}', 'ProfilesController@show');

