<?php 
// if there is no need for a title or a message 
function flash($title = null, $message = null) 

{
	// grabbing Flash at the contain 
	//This makes Flash a global function 
	
	$flash = app('App\Http\Flash');

	if(func_num_args() == 0){
		//return the flash instance out of the container 
		return $flash;
	}

	// return the message, a default message with just the title and the message 
	return $flash->info($title, $message);
}