<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;


class Service extends Model
{

    protected $table = 'services';

    protected $fillable = [
	    'name',
	    'short_description',
	    'price',
	    'rating'
    ];

    public function scopeLocatedAt($query, $name)
    {
        $name = str_replace('-', ' ' , $name);

        return $query->where(compact('name'));
    }

    /**
     * A service has many photos 
     */

    public function photos()
    {
      return $this->hasMany('App\Photo');
    }


    public function getPriceAttribute($price)
    {
         return '£' . number_format($price);

    }



 }
