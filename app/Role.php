<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{ 
    // the laravel function; 'protected $fillable' is sourced from the Eloquent model (JC)
    protected $fillable = ['username'];
}
