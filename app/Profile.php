<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Database\Eloquent\ModelNotFoundException;

class Profile extends Model {

     // the hasOne PHP function belongs to Laravel
    public function user($username)

     // one user == one profile, 'Profile' refers to the name of a model
    {
        return $this->hasOne('Profile');
    }
    
}
