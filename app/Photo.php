<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;


class Photo extends Model
{
	protected $table = 'service_photos'; 

	protected $fillable = ['path'];


    public function service()
    {

        return $this->belongsTo('App\Service');

    }




}