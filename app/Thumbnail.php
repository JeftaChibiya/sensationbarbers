<?php

namespace App;
use Image;

class Thumbnail
{   
	// create a thumbnail for an uploaded image using the Intervention/image library 

    public function make($src, $destination)
    {
        Image::make($src)
            ->fit(200)
            ->save($destination);
    }
}