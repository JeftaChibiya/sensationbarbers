@extends('layouts.app') <!--Bring layout from master.blade.php and make the layout of this page also-->
@section('title','Contact') <!--Take the title of this page, place before the global title of the site-->
@section('content')

<div class="container">
<ol class="breadcrumb">
  <label>Your are Here:&nbsp;</label>
  <li><a href="{{ url('/') }}">Home</a></li>
  <li><a href="contact">Contact</a></li>
</ol>
<h1 class="text-center side-lines"><span>Contact Us</span></h1>

<div class= "row slim">
<div class="col-md-6 white">
      <h2 class="text-center back">Sensation  Barbers</h2>
      <p class="text-center">239 Chapeltown</p>
      <p class="text-center">Leeds, Ls7 4EE</p>
      <p class="text-center"><span><i class="fa fa-phone fa-lg"></i></span>&nbsp;&nbsp;<span>07963 146 512</span></p>      
  </div><!--FIrst Div-->

<div class="col-md-6 first">
    <h2 class="text-center back">Opening Times</h2>
          <ul class="row">
              <li><span class="col-md-6 col-sm-6 col-xs-4">Monday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li> <!--The Laravel Blade template at work here as the equivalient of escaping the data and echoing it out-->
              <li><span class="col-md-6 col-sm-6  col-xs-4">Tuesday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li>
              <li><span class="col-md-6 col-sm-6  col-xs-4">Wednesday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li>
              <li><span class="col-md-6 col-sm-6  col-xs-4">Thursday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li>
              <li><span class="col-md-6 col-sm-6  col-xs-4">Friday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li>
              <li><span class="col-md-6 col-sm-6  col-xs-4">Saturday:</span><span class="col-md-6 col-sm-6 col-xs-6">11:30PM - 7:00PM</span></li>
              <li><span class="col-md-6 col-sm-6  col-xs-4">Sunday:</span><span class="col-md4 col-sm-6 col-xs-6">Closed</span></li>
    </ul>
</div>
</div><!-- row slim -->
  
<div class="row slim">
  <div class="col-md-6 white">
    <h2 class="text-center back">Send us a message</h2>
  <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" id="email" placeholder="Enter email">
          <div class="form-group">
          <label for="">Message:</label>
          <textarea type="text" class="form-control" id="text" placeholder="Your Message" rows="5"></textarea>
          </div>
          <button type="submit" class="btn btn-default">Send  </button>
          </form>
      </div>
</div><!--col-md-6-->
<div class="col-md-6">
  <h2 class="text-center back">We are here&nbsp;<i class="fa fa-map-marker fa-2x"></i></h2>
   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2355.511391122822!2d-1.5336140345888638!3d53.815977597131536!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48795bf5c83eecf7%3A0xfe13c5d097662da6!2sChapeltown+Rd%2C+Leeds%2C+West+Yorkshire+LS7+4EE!5e0!3m2!1sen!2suk!4v1457304110428" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div><!--narrowrow-->
</div>

@yield('scripts')
@stop