@extends('layouts.admin')
@section ('title', 'Services')
@section('mainTitle','Services')

@section('content')

    <?php foreach($services as $service):?>

    <h2><?php echo $service->name;  ?></h2>
    <h2><?php echo $service->description; ?></h2>

    <a href="/admin/services/{{ $service->id }}/edit" class="btn btn-primary">Edit</a>

    <a href="/admin/services/{{ $service->id }}" class="btn btn-success">View</a>
    <form action="{{ url('admin/services/'.$service->id) }}" method="POST">
     <!-- Delete method below, due to this method being a sensitive method, the !! csrf_field method is used protect unathourised access to this function -->
        {!! csrf_field() !!}
        {!! method_field('DELETE') !!}
       
        <button type="submit" id="delete-task-{{ $service->id }}" class="btn btn-danger">
            <i class="fa fa-btn fa-trash"></i>Delete
        </button>
    </form>
    <hr>

    <?php endforeach; ?>

@endsection