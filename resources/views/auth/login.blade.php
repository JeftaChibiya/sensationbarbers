
<!-- Created 15-03-2015, by Jefta Chibiya, C3398189 -->

    <div id="login-modal" class="modal fade" role="dialog" tabindex="-1">

        <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login to Your Account</h4>
             </div>

                <div class="modal-body">

                <form class="form-horizontal" method="POST" action="{{ url('/login') }}" novalidate>
                
                <!-- The 'Cross-site request forgery' or csrf method below protects the site against such attack of foregered or unprotected connections -->
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <!-- <label class="col-md-4 control-label">E-Mail Address</label> -->
                    <div class="col-md-10 col-md-offset-1">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                 <!-- <label class="col-md-4 control-label">Password</label> -->
                    <div class="col-md-10 col-md-offset-1">
                        <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <button type="submit" id="submit" class="btn btn-primary">
                            <i class="fa fa-sign-in"></i>Login
                        </button>
                    </div>
                </div>

            </form>
     <div class="modal-footer">
         <a class="btn btn-link text-left" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
          <a class="btn btn-link text-right" href="{{ url('/register') }}">Sign up!</a>
          </div>

        </div>
    </div>
    </div>
    </div>


