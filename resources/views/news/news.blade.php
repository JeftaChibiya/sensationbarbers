@extends('layouts.admin')
@section('title', 'News')
@section('mainTitle', 'News')

@section('content')

<?php foreach ($news as $news):?> {	
    <h2><?php echo $news->name;  ?></h2>
    <h2><?php echo $news->description;  ?></h2>

    <a href="/admin/news/{{ $news->id }}/edit" class="btn btn-primary">Edit</a>

    <a href="/admin/news/{{ $news->id }}" class="btn btn-success">View</a>

    <form action="{{ url('admin/services/'.$news->id) }}" method="POST">
        {!! csrf_field() !!}
        {!! method_field('DELETE') !!}

        <button type="submit" id="delete-task-{{ $news->id }}" class="btn btn-danger">
            <i class="fa fa-btn fa-trash"></i>Delete
        </button>
    </form>
    <hr>

    <?php endforeach; ?>
@endsection

}