@extends('layouts.app')
@section ('title', 'Profile')

@section('content')

<h1>{{ $username }}</h1>

	 <div class="bio">
	 	{{ $user->profile->bio }}
	 </div>

	 <ul class="links">
	 	<li>{{ link_to('http://twitter.com/' .$user->twitter_username, 'Share on twitter') }}</li>
	 </ul>


@endsection