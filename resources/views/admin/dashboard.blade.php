@extends('layouts.admin')
@section ('title', 'Dashboard')
@section('mainTitle','Dashboard')

@section('content')
 
    <div class="row">
    <div class="col-md-3">
		 <div class="users">
			  <p class="text-center">Number of Users: <span>{{ $users_count }}</span></p>
	   </div>
	</div>

   <div class="row">
   <p class=""><a href="users/create" class="btn btn-sm btn-primary"><i class="fa fa-plus-square-o"></i>
            Add New User</a></p>
	    <div class="table-responsive">
	        <table class="table table-striped" id="listing-table">
	            <thead>
	            <tr>
	                <th>Id</th>
	                <th>userame</th>
	                <th>email</th>
	                <th>Created on</th>
	                <th>Edit</th>
	                <th>Delete</th>
	                <th>View</th>
	            </tr>
	            </thead>
	        </table>
	    </div>   	
   </div>
</div>
@endsection



<!-- Datatable Script, Sourced with the permission of Arjay Angeles --> 
<!-- https://github.com/yajra/laravel-datatables-->
<!-- Sourced on: 17/04/2015 -->


