@extends ('layouts.app')
@section('title', 'Share')
@section ('content')

<div class="services-single">
    <div class="row">
    <h1><span>{{ $service->name }}</span></h1>
    
     <div class="col-md-5">
            <h2> {{ $service->price }} </h2>
            <p>{{ $service->short_description }}</p>
     </div>

    <div class="col-md-7">
            <h2 class="text-center">See your images here<i class="fa fa-arrow-down"></i></h2>
              @foreach($service->photos as $photo)
                  <img src="{{ $photo->path }}" alt="haircut">
              @endforeach
    </div>
  </div>

    <div class="row">

        <div class="col-md-5">
           <h2 class="text-center"><span class="label label-default">Drag and drop your photo(s) there <i class="fa fa-arrow-down"></i></span></h2>
            <!-- AddPhoto Form -->
            @include('partials.addPhotosform')

        </div>

        <div class="col-md-7">
            
        </div>
</div>
</div>

@endsection
