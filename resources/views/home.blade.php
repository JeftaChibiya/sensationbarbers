@extends('layouts.app')
@section ('title', 'Home')
@section('content')

    <div class="homepage">

         <!-- Welcome Banner -->
        @include('partials.banner')

         <!-- Services Section -->
        @include('partials.home-services')

         <!-- Barbers Section on Home page --> 
        @include('partials.home-barbers')

         <!-- News section on homepage -->
        @include('partials.home-news')

    </div>


@endsection
