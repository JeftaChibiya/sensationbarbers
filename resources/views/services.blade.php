@extends ('layouts.app')
@section ('title', 'Services')
@section('content')
    <div class="services-page">

    @include('partials.breadcrumbs')

    <h1 class="text-center side-lines"><span>OUR SERVICES</span></h1>

    <div class="row">   
          @foreach($services as $service)
            <div class="col-md-3">
                <div class="services">
                    <div class="title-bg"> 
                        <h3 class="title">{{ $service->service_name }}</h3> 
                    </div>
                    <div class="image-bg">  
                    <div class="image">      
                    </div>
              </div>     
            <a href="/service/{{ $service->id }}" class="btn btn-block btn-lg btn-primary">View</a>
        </div>
    </div>
          @endforeach

        <div class="col-md-3">
            <div class="services">
                    <div class="title-bg"> 
                        <h3 class="title">Share your style</h3>
                    </div>
                     <div class="image-bg">  
                         <div class="image">
                      </div>
                    </div>     
                 <a href="/service/show" class="btn btn-block btn-lg btn-primary"><i class="fa fa-upload">&nbsp;</i>Share a Photo</a>
             </div>
          </div>
        </div>

        @include('partials.feedback')
        @include('partials.faq')
    </div>
@endsection
