<div class="services-bg">

    <h1 class="text-center side-lines"><span>SERVICES</span></h1>

        @foreach($services as $service)
            <div class="col-md-3">
                <div class="service">
                    <div class="image">

                    </div>
                    <h4>{{  $service->service_name  }}</h4>
                </div>
            </div>
        @endforeach

</div>
