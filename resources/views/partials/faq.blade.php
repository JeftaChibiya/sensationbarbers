<div class="row">
<div class="col-md-8 col-md-offset-2">
 <h1 class="text-center side-lines"><span>Frequently Asked Questions</span></h1>
 <dl>
 	<dt>When are you open?</dt>
    <dd>10:30pm to 7:00pm on weekdays, closed on Sundays</dd>
 	<dt>What Services do you offer?</dt>
    <dd>10:30pm to 7:00pm on weekdays, closed on Sundays</dd>
 	<dt>Can I make an appointment in advance?</dt>
    <dd>10:30pm to 7:00pm on weekdays, closed on Sundays</dd>
 	<dt>If I contact you through the website, how long do you take to reply to enquiries?</dt>
    <dd>10:30pm to 7:00pm on weekdays, closed on Sundays</dd>
 </dl>
</div>
</div>