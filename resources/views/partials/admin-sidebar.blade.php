<ul class="nav nav-pills nav-stacked">

    <li class="nav-header text-center">MENU</li>
    <li role="presentation" class="active"><a href="{{ url('admin') }}">Dashboard</a></li>
    <li role="presentation"><a href="{{ url('admin/services') }}">Services</a></li>
    <li role="presentation"><a href="{{ url('admin/news') }}">News</a></li>
    <li role="presentation"><a href="{{ url('admin/bookings') }}">Bookings</a></li>
    <li role="presentation"><a href="{{ url('admin/users') }}">Users</a></li>
    
</ul>