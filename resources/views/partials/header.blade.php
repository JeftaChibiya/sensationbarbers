
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

    <!--Hamburger menu-->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#app-navbar-collapse">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <!-- Brand Image -->
    <a class="navbar-brand" href="{{ url('/') }}"><img id="brand-image" src="{{ url('images/sensbrand.png') }}"
                                                       alt="sensation barbers logo"></a>
    </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
    
    <!-- Right-side Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('services') }}">Services</a></li>
            <li><a href="{{ url('contact') }}">Contact</a></li>
            <li><a href="{{ url('news') }}">News</a></li>

        <!-- Authentication -->
        @if (Auth::guest())
            <button type="button" data-toggle="modal" data-target="#login-modal" class="two btn btn-default navbar-btn"><i class="fa fa-btn fa-sign-in"></i>&nbsp;Login
            </button>

            <button type="button" class="btn btn-default navbar-btn"><a
            href="{{ url('/register') }}">Register</a></button>

        @else
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>
                Hello &nbsp;{{ ucfirst(Auth::user()->username) }} <span class="caret"></span>
            </a>

                <ul class="dropdown-menu" role="menu">
                    @if(Auth::user()->hasRole('administrator'))
                        <li><a href="{{ url('/admin') }}"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                    @endif
                    <li><a href="{{ url('/profile') }}"><i class="fa fa-user"></i>Profile</a></li>
                    <li><a href="{{ url('/booking') }}"><i class="fa fa-check"></i> My Bookings</a></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>

        </div>
    </div>
</nav>
