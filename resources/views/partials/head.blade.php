<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') |Sensation Barbers</title>
    <!-- Favicon -->
    <link rel="icon" sizes="16x16" href="{{ asset('favicon.ico') }}">
    <!-- CSS files -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    
    <!-- CSS for flash messages, sweetalerts -->
    <link rel="stylesheet" href="/css/misc.css">
    <!-- Dropzone CSS CDN link -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">
    @yield('css')
</head>