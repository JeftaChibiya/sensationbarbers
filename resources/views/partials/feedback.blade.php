<!-- This is the inject method, which injects data from a class within the framework into a view -->
<!-- An alternative method is to referred to the path within a FeedbackController provided its created -->
@inject('feedback', 'App\Http\Utilities\Feedback')

<div class="row">
    <div class="feedback-grid">
        <div class="feedback-form">
            <h1 class="title text-center">We welcome your feedback</h1>
            <form method="POST" action="/layouts" enctype="mutlipart/form-data">

                <div class="form-group">
                    <label for="email">Your Email Address:</label>
                    <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label for="feedback">Feedback Category:</label>
                    <!-- Created: 17/02/2015, by JC -->
                    <select id="feedback" name="feedback" class="form-control">
                        @foreach ($feedback::all() as $feedback )
                            <option value="{{ $feedback }}">{{ $feedback }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="comments">Comments:</label>
                    <textarea type="text" name="comments" class="form-control" value="{{ old('comments') }}"
                              rows="5"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Submit Feedback</button>
                </div>
            </form>
        </div>
    </div>
</div>