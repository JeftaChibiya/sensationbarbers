<div class="banner-wrapper">

    <div class="row no-gutter">
        <div class="col-md-8">
            <div class="banner-bg">
                <div class="banner-image" style="background-image:url('../images/sensbg.png') ">
                 <a href="/services" class="button"><span>Browse Gallery</span></a>
                </div><!-- banner-image-->
            </div><!-- banner-bg -->
            </div> <!-- col-md-8-->

    <div class="col-md-4">
        <div class="opening-times-wrapper">
            <div class="opening-times-image-bg"> 

                <div class="opening-times-image" style="background-image: url('images/sensloc.jpeg')">

                    <div class="opening-times-info">
                    
                        <h2 class="text-center">CONTACT</h2>
                        <p class="text-center">Sensation Barbers</p>
                        <p class="text-center">239 Chapeltown, Leeds</p>
                        <p class="text-center">Leeds, Ls7 4EE</p>
                        <p class="text-center">07963 146 512</p>
                            <hr width="50%"/>
                            <h2 class="text-center">OPENING TIMES</h2>       

                <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Monday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Tuesday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Wednesday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Thursday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Friday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Saturday:</td>
                                <td>10:30am - 07:00pm</td>
                            </tr>
                            <tr>
                                <td>Sunday:</td>
                                <td>Closed</td>
                            </tr>
                    </tbody>
            </table>
            </div> <!-- div.table-responsive-->   
            <a href="{{ url('bookings.make') }}" class="button-two" role="button"><span>Book An Appointment</span></a>       
        </div><!-- opening-times-info-->
    </div><!--  openinng-times-image -->
</div><!-- opening-times-image-bg -->
</div><!-- opening-times-wrapper-->
</div> <!-- col-md-4 -->
</div><!-- row no-gutter -->
</div> <!-- div.banner-wrapper -->