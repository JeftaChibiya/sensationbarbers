<div class="news">
    <div class="background">
        <div class="row">
            <h1 class="text-center side-lines"><span>News</span></h1>
            @foreach($news as $news)
               <h4> {{ $news->title }} </h4>
               <p class="body">{{ str_limit( $news->body, 200) }}</p> 
                <p>{{ $news->created_at }}</p>
                <hr>
            @endforeach
        </div>
    </div>
</div>
