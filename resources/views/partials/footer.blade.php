<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>CONTACT</h2>
                <p>P: 07963 146 512</p>
                <p>239 Chapeltown Road,</p>
                <p>Leeds, LS7 4EE</p>
            </div>
            <div class="col-md-4">
                <h2>ALL SERVICES</h2>
                <p><a href="#">Drying</a></p>
                <p><a href="#">Texturising</a></p>
                <p><a href="#">Pattern Design</a></p>
                <p><a href="#">Fade</a></p>
                <p><a href="#">Afro-Caribbean & European Hair </a></p>
            </div>
            <div class="col-md-4">
                <h2>Visit us on</h2>
                <a href="https://www.facebook.com/pages/Sensation-Barbers/509069549166334" title="Facebook "><i
                            class="fa fa-facebook-official fa-2x">&nbsp;</i></a>
                &nbsp;&nbsp;<a href="https://twitter.com/sensationbarber" title="Twitter"><i
                            class="fa fa-twitter fa-2x">&nbsp;</i></a>
                <a href="https://twitter.com/sensationbarber" title="Instagram"><i class="fa fa-instagram fa-2x">
                        &nbsp;</i></a>
                <!-- To include an Apple and Google Store icon here on completing the smartphone ap-->
            </div>
        </div>
        <hr/>
        <p class="text-center">All content belongs to Sensation Barbers {{date('Y')}} &nbsp;|&nbsp;Designed and
            developed by <span class="chicle">JC</span></p>

    </div>
</div>


<!--             <div class="col-md-3">
                <h2>SENSATION SOUND SERVICES</h2>
                <p><a href="">Parties</a></p>
                <p><a href="">Christenings</a></p>
                <p><a href="">Weddings</a></p>
                <p><a href="">Events</a></p>
                <p><a href="">Rnb</a></p>
                <p> + More</p>
            </div>
            <div class="col-md-3">
                <h2>SENSATION SOUND SERVICES</h2>
                <p><a href="">Parties</a></p>
                <p><a href="">Christenings</a></p>
                <p><a href="">Weddings</a></p>
                <p><a href="">Events</a></p>
                <p><a href="">Rnb</a></p>
                <p> + More</p>
            </div>

                <h2>Other Websites</h2>
                <a href="#"><img src="../images/sens-music.png" class="grayscaled"></a><br/><br/>

            -->
