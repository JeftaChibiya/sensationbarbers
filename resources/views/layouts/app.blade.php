<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body id="app-layout">

@include('partials.header')	

<div class="container">

    @yield('content')
    
</div>

<!-- Footer -->
@include('partials.footer')


<!-- Versioned CSS below -->
<script src="{{ elixir('js/app.js') }}"></script>


@include('flash.show')

@yield('scripts')
     <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
      <script>
         Dropzone.options.addPhotosform ={
             paramName: 'photo',
             //3 megabites
             maxFilesize: 3, 
             acceptedFiles: '.jpg, .jpeg, .png, .bmp' 
         };
      </script>
</body>
</html>
