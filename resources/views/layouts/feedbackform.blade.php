@inject('feedback', 'App\Http\Utilities\feedback') 

 		<div class="form-group">
 			<label for="email">Your Email Address:</label>
 			<input type="text" name="email" class="form-control" value="{{ old('email') }}">
 		</div>

 		 <div class="form-group">
			<label for="feedback">Feedback Category:</label>
			<select id="feedback" name="feedback" class="form-control">			
				@foreach (App\Http\Utilities\feedback::all() as $feedback )
					<option value="{{ $feedback }}"></option>
				@endforeach					
			</select>
 		</div>
 		<div class="form-group">
			<label for="comments">Comments:</label>
			<textarea type="text" name="comments" class="form-control" value="{{ old('comments') }}" rows="5"></textarea>
 		</div>
 		<div class="form-group">
 			<button type="submit" class="btn btn-primary">Submit Feedback</button>
 		</div>
