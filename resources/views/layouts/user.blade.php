<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body id="app-layout">

	@include('partials.header') 
	<!-- Including navigation bar -->
	<!-- including language bar -->
	@include('partials.language')

<div class="container">
 <!-- Echo user's name -->
     @foreach ($users as $user)
     <div class="row">
	    <div class="col-md-3">
	        <h2>{{ $user->name }}</h2>
	    </div>
   </div>

	 @endforeach
	   <div class="row">
	    <div class="col-md-9">
	    <h2>No recent activity</h2>
	        <h1 class="text-center"><span>@yield('mainTitle')</span></h1>
	        @yield('content')
	    </div>
    </div>

</div>

@include('partials.footer')
<script src="{{ elixir('js/app.js') }}"></script>
@yield('js')
</body>
</html>
