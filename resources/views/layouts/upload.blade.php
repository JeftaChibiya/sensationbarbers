@extends('layout.app')
@section ('create', 'title')
@section ('content')

 <ol class="breadcrumb">
  <label>You are here:&nbsp;</label>
  <li><a href="home">Home</a></li>
  <li><a href="services">Services</a></li>
  <li><a href="/items/add">Share style</a></li> 
</ol> <!--Breadcrumb-->

<div class="row">
	<h1 class="side-lines"><span>Share your style</span></h1>
         <div class="editfrm col-md-offset-3 col-md-6 col-md-offset-3">
	         <form method="POST" action="/items" enctype="multipart/form-data">
	                   <div class="form-group">
		                   	<label for="style">Style name</label>
		                   	<select id="style" name="style" class="form-control">
		                   		<option>Fade</option>
		                   		<option>Mohawk</option>
		                   		<option>Texturised</option>
		                   	</select>
	                   </div>
	         	<div class="form-group">
	         		<label for="photos">Photo(s)</label>
	         		<input type="file" class="form-control" name="photos" value="{{ old('photos') }}">
	      		</div>
                   	<div class="form-group">
		         		<label for="description">Description</label>
		         		<textarea type="text" class="form-control" name="description" value="{{ old('description') }}" rows="3"></textarea>
         		</div>
	 		<div class="form-group">
	 			<button type="submit" class="btn btn-primary">Upload File&nbsp;</button>
	 		</div>
	         </form>

          </div>
     </div>
