<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body id="app-layout">
<!-- Including navigation bar -->
@include('partials.header') 

<!-- including language options -->
@include('partials.language')

<div class="container">
    
    <div class="col-md-3">
        @include('partials.admin-sidebar')
    </div>

    <div class="col-md-9">
        <h1 class="text-center"><span>@yield('mainTitle')</span></h1>
        @yield('content')
    </div>

</div>

@include('partials.footer')
<script src="{{ elixir('js/app.js') }}"></script>
@yield('js')
</body>
</html>
