@extends ('layouts.app')
@section ('title', 'News')
@section ('content')

    <div class="container">
        <ol class="breadcrumb">
            <label>You are here:&nbsp;</label>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="news">News</a></li>
        </ol>

        <h1 class="text-center side-lines"><span>Sensation Barbers & Culture</span></h1>
        <ul class="nav nav-tabs">
            <ul class="nav nav-pills">
                <li class="active"><a data-toggle="tab" href="#home">Music & Vids <span class="badge">8</span></a></li>
                <li><a data-toggle="pill" href="#menu1">Social Events <span class="badge">10</span></a></li>
                <li><a data-toggle="pill" href="#menu2">Trends <span class="badge">5</span></a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="row tab-pane fade in active">
                    <div class="col-md-3">
                        <h2>Check this out!</h2>
                        <label class="label label-default">New</label>
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item"
                                    src="https://www.youtube.com/embed/Ak1UioM4Rd0?list=PLY_7WJkKQUxAtNmTVymtanLghV-59UGaJ"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h2>Leed Carnival</h2>
                        <label class="label label-default">New</label>
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/oD9waUQzl9M"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h2>Leed Carnival</h2>
                        <label class="label label-default">New</label>
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/NBXx-mTb5YE"
                                    frameborder="0" allowfullscreen></iframe>
                            <h2>Share: <i class="fa fa-facebook-official fa-2x"></i>&nbsp;&nbsp;<i
                                        class="fa fa-twitter fa-2x"></i></h2>
                        </div>
                    </div><!--Last div second vid-->
                    <div class="col-md-3">
                        <h2>Leed Carnival</h2>
                        <label class="label label-default">New</label>
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/a_gkYEeleec"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div><!--Row-->
                <div id="menu1" class="tab-pane fade">
                    <h3>Menu 1</h3>
                    <img src="https://s-media-cache-ak0.pinimg.com/236x/4f/c4/16/4fc416548439f277f83f9c140c2cd6cb.jpg"
                         class="img-responsive"/>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h3>Menu 2</h3>
                    <p>Some content in menu 2.</p>
                </div>
            </div>
    </div>
    </div>

    </div>
    @yield('scripts')
@stop