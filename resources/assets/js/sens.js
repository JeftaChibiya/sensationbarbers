
// Highlight Active Link 

  
// toggling Frequently Asked Questions, 11-03-2016
// Courtesy of best practises by Jeffrey Way
// meaning: select any one of the 4th children of element 'dd', mouseenter do this for the 'next' child elements
//Notice I am chaining methods i.e. next.next, slideDown and others

(function() {
	var dd = $('dd');
	dd.filter(':nth-child(n+4)').hide();
	$('dt').on("mouseenter", function(){
	$(this)
	  .next()
	   .slideDown(200)
	     .siblings('dd')
		     .slideUp(200);
		 });
})();

// shake form if email and password do not match,
// courtesy of Agrawal Neeraj, founder of FormGet.com 
// function 'shake' is sourced from ../bower/jquery-ui



